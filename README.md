PriceAggregateBundle
==============

Enable the bundle
=================

Enable the bundle in the kernel:

```php
// app/AppKernel.php
public function registerBundles()
{
    $bundles = array(
        // ...
        new Nitra\PriceAggregateBundle\NitraPriceAggregateBundle(),
        // ...
    );
}
```

Import the routing
===================

Import the routing

```yaml
# app/config/routing.yml

NitraPriceAggregateBundle:
    resource: "@NitraPriceAggregateBundle/Resources/config/routing.yml"
    prefix:   /{_locale}/
    defaults:  { _locale: ru }
    requirements:
        _locale: en|ru
```

Add in menu translations
========================

 ```yaml
# app/Resources/translations/menu.en.yml
priceAggregate: "Settings to create a file for the price-aggregators"
```

```yaml
# app/Resources/translations/menu.en.yml
priceAggregate: "Настройки формирования файла для прайс-агрегаторов"
```

Add in menu
===========

```yaml
millwright_menu:
    items: #menu items
        priceAggregate:
            translateDomain: 'menu'    
            route: Nitra_PriceAggregateBundle_PriceAggregate_list
            
    tree: #menu containers
        main: #main container
            type: navigation # menu type id
            children:
                priceAggregate: ~
```